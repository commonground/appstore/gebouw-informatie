#bin/sh

perl -p -e 's/\$\{([^}]+)\}/defined $ENV{$1} ? $ENV{$1} : $&/eg; s/\$\{([^}]+)\}//eg' < android/app/src/main/AndroidManifest.xml > tempfile
cat tempfile > android/app/src/main/AndroidManifest.xml
rm tempfile
perl -p -e 's/\$\{([^}]+)\}/defined $ENV{$1} ? $ENV{$1} : $&/eg; s/\$\{([^}]+)\}//eg' < ios/Runner/AppDelegate.swift > tempfile
cat tempfile > ios/Runner/AppDelegate.swift
rm tempfile

import 'package:flutter/material.dart';
import 'package:gebouw_informatie/pages/home/home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    var themeColor = MaterialColor(0xFFFEB90E, color);
    return MaterialApp(
      title: 'Gebouw Informatie',
      theme: ThemeData(
          primarySwatch: themeColor ,
          buttonTheme: ButtonThemeData(
            buttonColor: themeColor[300],
            textTheme: ButtonTextTheme.primary,
          ),),
      home: MyHomePage(
        title: 'Gebouw Informatie',
      ),
    );
  }
}

Map<int, Color> color = {
  50: Color.fromRGBO(100, 73, 6, .1),
  100: Color.fromRGBO(100, 73, 6, .2),
  200: Color.fromRGBO(100, 73, 6, .3),
  300: Color.fromRGBO(100, 73, 6, .4),
  400: Color.fromRGBO(100, 73, 6, .5),
  500: Color.fromRGBO(100, 73, 6, .6),
  600: Color.fromRGBO(100, 73, 6, .7),
  700: Color.fromRGBO(100, 73, 6, .8),
  800: Color.fromRGBO(100, 73, 6, .9),
  900: Color.fromRGBO(100, 73, 6, 1),
};

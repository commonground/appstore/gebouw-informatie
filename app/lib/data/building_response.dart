import 'package:gebouw_informatie/data/buildings_model.dart';

class BuildingsResponse {
  List<BuildingsModel> buildings;

  BuildingsResponse({this.buildings});

  BuildingsResponse.fromJson(Map<String, dynamic> json) {
    if (json['Buildings'] != null) {
      buildings = new List<BuildingsModel>();
      json['Buildings'].forEach((v) {
        buildings.add(new BuildingsModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.buildings != null) {
      data['Buildings'] = this.buildings.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

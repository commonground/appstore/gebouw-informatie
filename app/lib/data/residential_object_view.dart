import 'package:gebouw_informatie/data/residential_object_model.dart';

class ResidentialObjectView {
  ResidentialObjectModel residentialObjectModel;
  String status;
  int constructionYear;

  ResidentialObjectView({this.residentialObjectModel, this.status, this.constructionYear});

}

class CoordinateModel {
  double lat;
  double long;

  CoordinateModel({this.lat, this.long});

  CoordinateModel.fromJson(Map<String, dynamic> json) {
    lat = json['Lat'];
    long = json['Long'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Lat'] = this.lat;
    data['Long'] = this.long;
    return data;
  }
}

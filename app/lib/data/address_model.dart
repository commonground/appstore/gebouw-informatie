class AddressModel {
  String address;
  String number;
  String postalCode;

  AddressModel({this.address, this.number, this.postalCode});

  AddressModel.fromJson(Map<String, dynamic> json) {
    address = json['Address'];
    number = json['Number'];
    postalCode = json['PostalCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Address'] = this.address;
    data['Number'] = this.number;
    data['PostalCode'] = this.postalCode;
    return data;
  }
}

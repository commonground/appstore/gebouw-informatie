import 'dart:convert';

import 'package:gebouw_informatie/data/building_response.dart';
import 'package:http/http.dart' as http;

class ApiClient {
  final String url;

  ApiClient(this.url);

  Future<BuildingsResponse> getBuildings(
      double latitude, double longitude) async {
    var response =
        await http.get("${this.url}/building?lat=$latitude&lng=$longitude");
    if (response.statusCode == 200) {
      return BuildingsResponse.fromJson(jsonDecode(response.body));
    }

    throw "failed to load buildings";
  }
}

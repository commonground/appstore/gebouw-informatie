import 'package:gebouw_informatie/data/address_model.dart';
import 'package:gebouw_informatie/data/coordinate_model.dart';

class ResidentialObjectModel {
  int surfaceSize;
  AddressModel address;
  CoordinateModel coordinate;

  ResidentialObjectModel({this.surfaceSize, this.address, this.coordinate});

  ResidentialObjectModel.fromJson(Map<String, dynamic> json) {
    surfaceSize = json['SurfaceSize'];
    address =
        json['Address'] != null ? new AddressModel.fromJson(json['Address']) : null;
    coordinate = json['Coordinate'] != null
        ? new CoordinateModel.fromJson(json['Coordinate'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['SurfaceSize'] = this.surfaceSize;
    if (this.address != null) {
      data['Address'] = this.address.toJson();
    }
    if (this.coordinate != null) {
      data['Coordinate'] = this.coordinate.toJson();
    }
    return data;
  }
}

import 'package:gebouw_informatie/data/residential_object_model.dart';

class BuildingsModel {
  String status;
  int constructionYear;
  List<ResidentialObjectModel> residentialObjects;

  BuildingsModel({this.status, this.constructionYear, this.residentialObjects});

  BuildingsModel.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    constructionYear = json['ConstructionYear'];
    if (json['ResidentialObjects'] != null) {
      residentialObjects = new List<ResidentialObjectModel>();
      json['ResidentialObjects'].forEach((v) {
        residentialObjects.add(new ResidentialObjectModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    data['ConstructionYear'] = this.constructionYear;
    if (this.residentialObjects != null) {
      data['ResidentialObjects'] =
          this.residentialObjects.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

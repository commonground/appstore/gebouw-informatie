import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  final VoidCallback loginCallback;

  LoginPage({this.loginCallback});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Gebouw informatie",
          style:
              Theme.of(context).textTheme.title.copyWith(color: Colors.white),
        ),
      ),
      body: Center(
        child: RaisedButton(
          color: Color(0xFFFEB90E),
          child: Text(
            "Login",
            style: Theme.of(context)
                .textTheme
                .button
                .copyWith(color: Colors.white),
          ),
          onPressed: () {
            loginCallback();
          },
        ),
      ),
    );
  }
}

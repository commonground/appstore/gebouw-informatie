import 'package:flutter/material.dart';

class Address extends StatelessWidget {
  final String address;
  final VoidCallback onTapCallback;

  Address({this.address, this.onTapCallback});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTapCallback,
      trailing: Icon(
        Icons.chevron_right,
        color: Colors.grey,
      ),
      title: Text(address),
    );
  }
}

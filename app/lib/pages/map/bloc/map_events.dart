class MapEvent {}

class NewLocation extends MapEvent {
  double latitude;
  double longitude;

  NewLocation(this.latitude, this.longitude);
}


import 'package:gebouw_informatie/data/residential_object_view.dart';

class MapState {
  List<ResidentialObjectView> residentialObjects;
  bool isLoading;
  bool hasError;

  MapState({this.residentialObjects
  , this.isLoading, this.hasError});

  MapState copyWith({List<ResidentialObjectView> residentialObjects
  , bool isLoading, bool hasError}) => MapState(
        residentialObjects
        : residentialObjects
         ?? this.residentialObjects
        ,
        isLoading: isLoading ?? this.isLoading,
        hasError: hasError ?? this.hasError,
      );
}

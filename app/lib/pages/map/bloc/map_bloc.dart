import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gebouw_informatie/data/api_client.dart';
import 'package:gebouw_informatie/data/residential_object_view.dart';
import 'package:gebouw_informatie/pages/map/bloc/map_events.dart';
import 'package:gebouw_informatie/pages/map/bloc/map_state.dart';

class MapBloc extends Bloc<MapEvent, MapState> {
  final ApiClient apiClient;

  MapBloc(this.apiClient);

  @override
  MapState get initialState => MapState(
        residentialObjects: <ResidentialObjectView>[],
        isLoading: false,
        hasError: false,
      );

  @override
  Stream<MapState> mapEventToState(MapEvent event) async* {
    if (event is NewLocation) {
      if (!state.isLoading) {
        yield state.copyWith(isLoading: true, hasError: false);
        try {
          var response =
              await apiClient.getBuildings(event.latitude, event.longitude);

          List<ResidentialObjectView> residentialObjects = List();
          response.buildings.forEach((building) {
            building.residentialObjects.forEach((residentialObject) {
              residentialObjects.add(ResidentialObjectView(
                residentialObjectModel: residentialObject,
                status: building.status,
                constructionYear: building.constructionYear,
              ));
            });
          });

          yield state.copyWith(
              isLoading: false, residentialObjects: residentialObjects);
        } catch (_) {
          yield state.copyWith(isLoading: false, hasError: true);
        }
      }
    }
  }
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gebouw_informatie/data/address_model.dart';
import 'package:gebouw_informatie/data/api_client.dart';
import 'package:gebouw_informatie/pages/map/bloc/map_bloc.dart';
import 'package:gebouw_informatie/pages/map/bloc/map_events.dart';
import 'package:gebouw_informatie/pages/map/bloc/map_state.dart';
import 'package:gebouw_informatie/pages/map/widgets/address.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapPage extends StatefulWidget {
  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  Completer<GoogleMapController> _controller = Completer();
  Bloc _bloc;
  bool _bottomSheetVisible = false;
  PersistentBottomSheetController bottomSheet;
  

  static final CameraPosition _groningenCentrum = CameraPosition(
    target: LatLng(53.2172087, 6.5654734),
    zoom: 15,
  );

  @override
  void initState() {
    _bloc = MapBloc(ApiClient("https://gebouw-informatie-api.demoground.nl"));
    super.initState();
  }

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        title: Text(
          "Gebouw informatie",
          style: Theme.of(context).textTheme.title.copyWith(
                color: Colors.white,
              ),
        ),
      ),
      body: BlocBuilder<MapBloc, MapState>(
        bloc: _bloc,
        builder: (context, state) {
          return Column(
            children: <Widget>[
              Expanded(
                child: GoogleMap(
                  mapType: MapType.normal,
                  initialCameraPosition: _groningenCentrum,
                  myLocationButtonEnabled: false,
                  onTap: (position) async {
                    if (_bottomSheetVisible) {
                      Navigator.of(context).pop();
                    }
                    _bloc.add(NewLocation(position.latitude,
                        position.longitude));
                  },
                  onMapCreated: (GoogleMapController controller) {
                    _controller.complete(controller);
                  },
                ),
              ),
              Container(
                height: 200,
                padding: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.rectangle,
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 3.0,
                        spreadRadius: 1.0,
                      ),
                    ]),
                child: state.hasError
                    ? _buildErrorWidget()
                    : state.isLoading
                        ? _buildLoadingWidget()
                        : state.residentialObjects.length == 0
                            ? _buildNoDataWidget()
                            : ListView.separated(
                                separatorBuilder: (context, index) {
                                  return Padding(
                                    padding: const EdgeInsets.only(
                                        left: 16.0, right: 24.0),
                                    child: Divider(
                                      color: Colors.grey[500],
                                    ),
                                  );
                                },
                                itemCount: state.residentialObjects.length,
                                itemBuilder: (context, index) {
                                  var residentialObject =
                                      state.residentialObjects[index];
                                  var adressModel = residentialObject
                                      .residentialObjectModel.address;
                                  return Address(
                                    address:
                                        "${adressModel.address} ${adressModel.number}",
                                    onTapCallback: () {
                                      var objectPostition = residentialObject
                                          .residentialObjectModel.coordinate;
                                      _goToPosition(LatLng(
                                        objectPostition.lat,
                                        objectPostition.long,
                                      ));
                                      _bottomSheetVisible = true;
                                      showBottomSheet(
                                          context: context,
                                          builder: (context) =>
                                              _buildBottomSheet(
                                                context,
                                                adressModel,
                                                residentialObject.status,
                                                residentialObject
                                                    .residentialObjectModel
                                                    .surfaceSize,
                                                residentialObject
                                                    .constructionYear,
                                              )).closed.then((_) {
                                        _bottomSheetVisible = false;
                                      });
                                    },
                                  );
                                },
                              ),
              )
            ],
          );
        },
      ),
    );
  }

  Widget _buildErrorWidget() {
    return Center(
      child: Text("Er is iets fout gegaan :("),
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
        CircularProgressIndicator(),
        SizedBox(height: 8),
        Text("Adressen aan het ophalen")
      ]),
    );
  }

  Widget _buildNoDataWidget() {
    return Center(
      child: Text("Geen data gevonden voor deze locatie"),
    );
  }

  Widget _buildBottomSheet(BuildContext context, AddressModel addressModel,
      String status, int surfaceSize, int constructionYear) {
    const double padding = 16.0;
    return Container(
      width: MediaQuery.of(context).size.width,
      color: Colors.white,
      height: 200,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              color: Color(0xFFFEB90E),
              child: Padding(
                  padding: const EdgeInsets.all(padding),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "${addressModel.address} ${addressModel.number}",
                        style: Theme.of(context)
                            .textTheme
                            .headline
                            .copyWith(color: Colors.white),
                      ),
                      Text("${addressModel.postalCode}",
                          style: Theme.of(context)
                              .textTheme
                              .subtitle
                              .copyWith(color: Colors.white)),
                    ],
                  )),
              width: MediaQuery.of(context).size.width,
            ),
            Padding(
              padding: const EdgeInsets.only(left: padding),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: padding,
                  ),
                  Text("Status: '$status'"),
                  Text("Bouwjaar: $constructionYear"),
                  Text("Opervlakte: ${surfaceSize}m")
                ],
              ),
            ),
          ]),
    );
  }

  Future<void> _goToPosition(LatLng position) async {
    final GoogleMapController gmController = await _controller.future;
    gmController.animateCamera(CameraUpdate.newLatLngZoom(position, 20));
  }
}

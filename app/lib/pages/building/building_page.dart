import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class BuildingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Gebouw informatie"),
      ),
      body: Column(
        children: <Widget>[
          Container(
            height: 200,
            child: GoogleMap(
              scrollGesturesEnabled: false,
              zoomGesturesEnabled: false,
              rotateGesturesEnabled: false,
              tiltGesturesEnabled: false,
              myLocationEnabled: false,
              mapType: MapType.hybrid,
              initialCameraPosition: CameraPosition(
                target: LatLng(53.2172087, 6.5654734),
                zoom: 19,
              ),
            ),
          ),
          SizedBox(height: 8),
          Column(children: <Widget>[
            ListTile(
              leading: Icon(Icons.location_on),
              title: Text("Guldenstraat 42"),
              subtitle: Text("9712 CG, Groningen"),
            ),
            //  ListTile(
            //   leading: Icon(Icons.person),
            //   title: Text("Ronald Koster"),
            // ),
            // ListTile(
            //   leading: Icon(Icons.monetization_on),
            //   title: Text("200.000"),
            // ),
          ],),
        ],
      ),
    );
  }
}

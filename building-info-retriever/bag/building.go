package bag

type BuildingSearchRequest struct {
	Geometry GeometryContains `json:"geometrie"`
}

type BuildingSearchResponse struct {
	Embedded EmbeddedBuilding `json:"_embedded"`
}

type EmbeddedBuilding struct {
	Buildings []BuildingResponse `json:"panden"`
}

type BuildingResponse struct {
	ConstructionYear int                   `json:"oorspronkelijkBouwjaar"`
	Status           string                `json:"status"`
	Links            BuildingResponseLinks `json:"_links"`
}

type BuildingResponseLinks struct {
	Self               Link `json:"self"`
	ResidentialObjects Link `json:"verblijfsobjecten"`
}

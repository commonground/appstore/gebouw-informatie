package bag

type Geometry struct {
	Coordinates Coordinate `json:"coordinates"`
}

type GeometryContains struct {
	Contains Contains `json:"contains"`
}

type Contains struct {
	Type        string     `json:"type"`
	Coordinates Coordinate `json:"coordinates"`
}

type Coordinate [2]float64

type Link struct {
	Href string `json:"href"`
}

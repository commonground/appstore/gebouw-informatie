package bag

type ResidentialObjectsSearchResponse struct {
	Embedded EmbeddedResidentialObjects `json:"_embedded"`
}

type EmbeddedResidentialObjects struct {
	ResidentialObjects []ResidentialObject `json:"verblijfsobjecten"`
}

type ResidentialObject struct {
	SurfaceSize int                       `json:"oppervlakte"`
	Links       ResidentialObjectLinks    `json:"_links"`
	Embedded    ResidentialObjectEmbedded `json:"_embedded"`
}

type ResidentialObjectLinks struct {
	MainAddress Link `json:"hoofdadres"`
}

type ResidentialObjectEmbedded struct {
	Geometry Geometry `json:"geometrie"`
}

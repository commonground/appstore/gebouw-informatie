package bag

type NumberIndicationResponse struct {
	HouseNumber int                   `json:"huisnummer"`
	HouseLetter string                `json:"huisletter"`
	PostalCode  string                `json:"postcode"`
	Links       NumberIndicationLinks `json:"_links"`
}

type NumberIndicationLinks struct {
	PublicSpace Link `json:"bijbehorendeOpenbareRuimte"`
}

package buildinginfo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"buildinginfo/bag"
)

const OutwayHost = "nlx-outway"
const BasePath = "/hackathon-services/kadaster-bag"
const APIKey = "6b017b0c-24c2-408b-81f2-4f09e8806c64"

const GeoLookupServerEndpoint = "https://geodata.nationaalgeoregister.nl/locatieserver/revgeo"

// RetrieveBuildingInfo info retrieves building info of a
func RetrieveBuildingInfo(w http.ResponseWriter, r *http.Request) {
	lat, err := strconv.ParseFloat(r.URL.Query().Get("lat"), 64)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	lng, err := strconv.ParseFloat(r.URL.Query().Get("lng"), 64)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	searchResponse := searchForBuildings(lat, lng)

	buildings := make([]Building, len(searchResponse.Embedded.Buildings))

	for i, b := range searchResponse.Embedded.Buildings {

		ror := fetchResidentialObjects(b.Links.ResidentialObjects.Href)

		residents := make([]ResidentialObject, len(ror))

		for i, r := range ror {
			ni, _ := fetchNumberIndication(r.Links.MainAddress.Href)
			ps, _ := fetchPublicSpace(ni.Links.PublicSpace.Href)

			residents[i] = ResidentialObject{
				SurfaceSize: r.SurfaceSize,
				Address: Address{
					Address:    ps.Name,
					Number:     strconv.Itoa(ni.HouseNumber) + ni.HouseLetter,
					PostalCode: ni.PostalCode,
				},
				Coordinate: Coordinate{
					Lat:  r.Embedded.Geometry.Coordinates[1],
					Long: r.Embedded.Geometry.Coordinates[0],
				},
			}
		}

		building := Building{
			Status:             b.Status,
			ConstructionYear:   b.ConstructionYear,
			ResidentialObjects: residents,
		}

		buildings[i] = building
	}

	br := BuildingsResponse{
		buildings,
	}

	data, _ := json.Marshal(br)

	w.Write(data)

	// if address == "" {
	// 	w.WriteStatus(http.StatusBadRequest)
	// 	return
	// }
}

type BuildingsResponse struct {
	Buildings []Building
}

type Building struct {
	Status             string
	ConstructionYear   int
	ResidentialObjects []ResidentialObject
}

type ResidentialObject struct {
	SurfaceSize int
	Address     Address
	Coordinate  Coordinate
}

type Coordinate struct {
	Lat  float64
	Long float64
}

type Address struct {
	Address    string
	Number     string
	PostalCode string
}

func searchForBuildings(lat, lng float64) *bag.BuildingSearchResponse {
	sbr := &bag.BuildingSearchRequest{
		Geometry: bag.GeometryContains{
			Contains: bag.Contains{
				Type:        "Point",
				Coordinates: bag.Coordinate{lat, lng},
			},
		},
	}

	body, err := json.Marshal(sbr)

	if err != nil {
		fmt.Println(err)
		return nil
	}

	response, err := createRequest("POST", getUrl("/panden"), bytes.NewReader(body))

	if err != nil {
		fmt.Println(err)
		return nil
	}

	responseBody, _ := ioutil.ReadAll(response.Body)
	response.Body.Close()

	var br bag.BuildingSearchResponse

	err = json.Unmarshal(responseBody, &br)

	if err != nil {
		fmt.Println(err)
		return nil
	}

	return &br
}

func rewriteForOuway(u string) string {
	p, _ := url.Parse(u)

	r := strings.Replace(p.Path, "/api/v1", "", 1)
	r = r + "?" + p.Query().Encode()

	return r
}

func fetchResidentialObjects(rawUrl string) []bag.ResidentialObject {
	u := rewriteForOuway(rawUrl)

	response, err := createRequest("GET", getUrl(u), nil)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	var r bag.ResidentialObjectsSearchResponse

	err = parseBody(response, &r)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return r.Embedded.ResidentialObjects
}

func formatGeoFloat(f float64) string {
	return strconv.FormatFloat(f, 'f', 5, 64)
}

func fetchNumberIndication(addressUrl string) (*bag.NumberIndicationResponse, error) {
	u := rewriteForOuway(addressUrl)

	response, err := createRequest("GET", getUrl(u), nil)
	if err != nil {
		return nil, err
	}

	var r *bag.NumberIndicationResponse

	err = parseBody(response, &r)

	if err != nil {
		return nil, err
	}

	return r, nil
}

func fetchPublicSpace(spaceUrl string) (*bag.PublicSpaceResponse, error) {
	u := rewriteForOuway(spaceUrl)

	response, err := createRequest("GET", getUrl(u), nil)
	if err != nil {
		return nil, err
	}

	var r *bag.PublicSpaceResponse

	err = parseBody(response, &r)

	if err != nil {
		return nil, err
	}

	return r, nil
}

func getUrl(path string) string {
	return "http://" + OutwayHost + BasePath + path
}

func createRequest(method, u string, body io.Reader) (*http.Response, error) {
	req, err := http.NewRequest(method, u, body)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/hal+json")
	req.Header.Set("X-Api-Key", APIKey)

	return http.DefaultClient.Do(req)
}

func parseBody(r *http.Response, t interface{}) error {
	body, _ := ioutil.ReadAll(r.Body)
	r.Body.Close()

	return json.Unmarshal(body, t)
}

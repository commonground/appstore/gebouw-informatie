package main

import (
	"net/http"

	"github.com/go-chi/chi"

	"buildinginfo"
)

func main() {
	r := chi.NewRouter()
	r.Get("/building", buildinginfo.RetrieveBuildingInfo)
	http.ListenAndServe(":3000", r)
}
